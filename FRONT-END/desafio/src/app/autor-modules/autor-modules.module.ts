import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenubarModule} from 'primeng/menubar';
import {TableModule} from 'primeng/table';
import {MenuItem} from 'primeng/api';
import { ListagemDeAutoresComponent } from './listagem-de-autores/listagem-de-autores.component';


export class Autor{
  nome: string;
	
	sexo: Sexo ;
	
	email: string;
	
	dataNascimento: Date;
	
	paisDeOrigem: PaisOrigem ;
	
	cpf: string ;
}

enum Sexo{
  MASCULINO,
  FEMININO
}

enum PaisOrigem{
  BRASIL,
  ARGENTINA,
  FRANCA
}

@NgModule({
  declarations: [
    ListagemDeAutoresComponent
  ],
  imports: [
    CommonModule,
    MenubarModule,
    TableModule
  ],
  exports: [
    ListagemDeAutoresComponent
  ]
})
export class AutorModulesModule { }
