import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDeAutoresComponent } from './listagem-de-autores.component';

describe('ListagemDeAutoresComponent', () => {
  let component: ListagemDeAutoresComponent;
  let fixture: ComponentFixture<ListagemDeAutoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDeAutoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDeAutoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
