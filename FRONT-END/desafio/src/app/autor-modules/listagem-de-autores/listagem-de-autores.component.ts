import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Autor } from '../autor-modules.module';
import { AutorService } from '../autor.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listagem-de-autores',
  templateUrl: './listagem-de-autores.component.html',
  styleUrls: ['./listagem-de-autores.component.css']
})
export class ListagemDeAutoresComponent implements OnInit {

  constructor(private autor: AutorService) { }

  itens: MenuItem[];

  autores: [];

  cols: any[];

    ngOnInit() { 
        // this.autor.buscarTodos().then(res => {this.autores = res,console.log(res)});
        this.autor.buscarTodos()
        .then( dados => {
            this.autores = dados;
        });
        this.itens = [
            {
                label: 'Autor',
                items: [{
                        label: 'Cadastrar', 
                        icon: 'pi pi-fw pi-plus',
                    },
                    {label: ' Listar',
                     icon: 'pi pi-list',
                  }
                ]
            },
            {
                label: 'Obra',
                items: [{
                          label: 'Cadastrar', 
                          icon: 'pi pi-fw pi-plus',
              },
                    {label: ' Listar',
                     icon: 'pi pi-list',
                  }
              ]
            }
        ];
        
            this.cols = [
                { field: 'nome', header: 'Nome' },
                { field: 'sexo', header: 'Sexo' },
                { field: 'email', header: 'Email' },
                { field: 'dataNascimento', header: 'Data de Nascimento' },
                { field: 'paisDeOrigem', header: 'Pais de Origem' },
                { field: 'cpf', header: 'CPF' }
            ];
        }
    }

