import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autor } from './autor-modules.module';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AutorService {

  AUTOR_URL = 'http://localhost:8080/autor'

  constructor(private http: HttpClient) { }

  buscarTodos(): Promise<any>{
    return this.http.get(`http://localhost:8080/autor`)
    .toPromise()
    //.then(response => response);
    .then(response => response.valueOf);
  }

}
