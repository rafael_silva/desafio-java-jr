import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutorModulesModule } from './autor-modules/autor-modules.module';
import { HttpClientModule } from '@angular/common/http';

import { ListagemDeAutoresComponent } from './autor-modules/listagem-de-autores/listagem-de-autores.component';
import { AutorService } from './autor-modules/autor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AutorModulesModule,
    HttpClientModule
  ],
  providers: [AutorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
