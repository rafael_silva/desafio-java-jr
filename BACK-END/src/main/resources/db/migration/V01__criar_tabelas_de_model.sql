CREATE TABLE autor(
	id BIGINT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome varchar(90) NOT NULL,
    sexo varchar(30),
    email varchar(90),
    data_nascimento date,
    pais_de_origem varchar(50),
    cpf varchar(30)
    
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('rafael', 'rafael.ruts', '1998-05-16', 'BRASIL', '343.343.343-43');
insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('antonio', 'antonio@gmail', '1998-12-16', 'FRANCA', NULL);
insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('nikson', 'nikson123', '1998-05-23', 'BRASIL', '343.343.343-11');
insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('emanuel', 'emanuel121', '1998-05-11', 'ARGENTINA', NULL);
insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('tamires', 'tamires444', '1998-05-12', 'BRASIL', '343.343.343-22');
insert into autor(nome, email, data_nascimento, pais_de_origem, cpf) values ('teste1', 'teste1111', '1998-05-15', 'BRASIL', '343.343.343-55');

CREATE TABLE obra(
	id BIGINT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome varchar(90) NOT NULL,
    descricao varchar(240) NOT NULL,
    data_de_publicacao date,
    data_de_exposicao date
    
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE autor_obras(
	autor_id BIGINT,
    obra_id BIGINT,
	primary key (autor_id,obra_id),
    foreign key (autor_id) references autor(id),
    foreign key (obra_id) references obra(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;



