package com.example.teste.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.teste.exceptionhandler.DesafioException;
import com.example.teste.model.Autor;
import com.example.teste.model.Obra;
import com.example.teste.repository.AutorRepository;
import com.example.teste.repository.ObraRepository;
@Service
public class AutorService {
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private ObraRepository obraRepository;
	
	@PersistenceContext
	private EntityManager manager;
	
	
	public Autor save(Autor autor) throws DesafioException {
		if(!validarAutor(autor)) {
			throw new DataIntegrityViolationException("campos obrigatorios não preenchidos ou inválidos");
		}
		if(autor.getObras().size()!=0) {
			autorRepository.save(autor);
			
			//salvar os Obras que foram cadastrados juntamente com o Autor
			for (Obra obra : autor.getObras()) {
				obra.addAutor(autor);
				obraRepository.save(obra);
				autor.addObra(obra);
			}
		}
		
		return autorRepository.save(autor);
	}
	
	private boolean validarAutor(Autor autor) throws DesafioException {
		if( autor.getPaisDeOrigem()==null || !getDateTime(autor) || autor.getPaisDeOrigem().getNome() == "Brasil" && autor.getCpf()==null ) {
			return false;
		}else if(!validarCPF(autor)) {
			return false;
		}
		if(autor.getEmail()!=null){
			if(!validarEmail(autor)) {
				return false;
			}
		}
		return true;
	}
	
	
	public Autor atualizar(Long codigo, Autor autor) throws DesafioException {
		Autor autorSalvo= buscarPeloCodigo(codigo);
		if(autorSalvo==null) {
			throw new EmptyResultDataAccessException(1);
		}
		if(!validarAutor(autor)) {
			throw new DataIntegrityViolationException("campos obrigatorios não preenchidos ou inválidos");
		}
		BeanUtils.copyProperties(autor, autorSalvo, "id");
		return autorRepository.save(autorSalvo);
	}
	
	public Autor buscarPeloCodigo(Long codigo) {
		Autor autorSalvo= autorRepository.findOne(codigo);
		if(autorSalvo==null) {
			throw new EmptyResultDataAccessException(1);
		}
		return autorSalvo;
	}
	
	public List<Autor> findAll(){
		return autorRepository.findAll();
	}
	
	public void delete(Long codigo) {
		Autor autorSalvo= autorRepository.findOne(codigo);
		if(autorSalvo==null) {
			throw new EmptyResultDataAccessException(1);
		}
		autorRepository.delete(codigo);
	}
	
	public Autor addObra(Long codigo, Long codigoObra) {
		Autor autorSalvo = buscarPeloCodigo(codigo);
		Obra obra=buscarObra(codigoObra);
		obra.addAutor(autorSalvo);
		autorSalvo.addObra(obra);
		return autorRepository.save(autorSalvo);
	}
	
	private Obra buscarObra(Long codigo) {
		Obra obraSalva= obraRepository.findOne(codigo);
		if(obraSalva==null) {
			throw new EmptyResultDataAccessException(1);
		}
		return obraSalva;
	}
	
	private static boolean getDateTime(Autor autor) throws DesafioException {
		if(autor.getDataNascimento()!=null) {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			String a = dateFormat.format(date);
			
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Date data = null;
			try {
				data = formato.parse(a);
			} catch (ParseException e) {
				throw new DesafioException(e.getLocalizedMessage());
			}
			if (autor.getDataNascimento().before(data)) {
				return true;
			}
		}
		throw new DesafioException("Data de Nascimento Inválida");
	}
	
	private boolean validarCPF(Autor autor) throws DesafioException {
		if (autor == null || autor.getCpf() == null) {
			return false;
		}

		String jpql = "SELECT COUNT(*) FROM Autor a WHERE a.cpf = :cpf";
		if (autor.getId() != null) {
			jpql += "AND a != :autor ";
		}

		TypedQuery<Long> query = manager.createQuery(jpql, Long.class);

		query.setParameter("cpf", autor.getCpf());
		if (autor.getId() != null) {
			query.setParameter("autor", autor);
		}

		Long count = query.getSingleResult();
		if (count > 0) {
			throw new DesafioException("O CPF informado, já está cadastrado");
		}
		return true;

	}
	
	private boolean validarEmail(Autor autor) throws DesafioException {
		if (autor == null || autor.getEmail() == null) {
			return false;
		}

		String jpql = "SELECT COUNT(*) FROM Autor a WHERE a.email = :email";
		if (autor.getId() != null) {
			jpql += "AND a != :autor ";
		}

		TypedQuery<Long> query = manager.createQuery(jpql, Long.class);

		query.setParameter("email", autor.getEmail());
		if (autor.getId() != null) {
			query.setParameter("autor", autor);
		}

		Long count = query.getSingleResult();
		if (count > 0) {
			throw new DesafioException("O Email informado, já está cadastrado");
		}
		return true;

	}


}
