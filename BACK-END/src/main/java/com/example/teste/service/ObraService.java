package com.example.teste.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.teste.model.Autor;
import com.example.teste.model.Obra;
import com.example.teste.repository.AutorRepository;
import com.example.teste.repository.ObraRepository;
import com.example.teste.repository.filter.ObraFilter;

@Service
public class ObraService {

	@Autowired
	private ObraRepository obraRepository;

	@Autowired
	private AutorRepository autorRepository;

	public Obra save(Obra obra) {
		if (!validarObra(obra)) {
			throw new DataIntegrityViolationException("campos obrigatorios nao preenchidos");
		} else {
//			obraRepository.save(obra);
//
//			for (Autor autor : obra.getAutores()) {
//				autor.addObra(obra);
//				autorRepository.save(autor);
//				obra.addAutor(autor);
//			}
			return obraRepository.save(obra);
		}
	}

	private boolean validarObra(Obra obra) {
		if (obra.getDataDeExposicao() != null || obra.getDataDePublicacao() != null && obra.getAutores().size() != 0) {
			return true;
		}
		return false;
	}

	public Obra atualizar(Long codigo, Obra obra) {
		Obra obraSalvo = buscarPeloCodigo(codigo);
		if (obraSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		if (!validarObra(obra)) {
			throw new DataIntegrityViolationException("campos obrigatorios nao preenchidos");
		}
		BeanUtils.copyProperties(obra, obraSalvo, "id");
		return obraRepository.save(obraSalvo);
	}

	public Obra buscarPeloCodigo(Long codigo) {
		Obra obraSalvo = obraRepository.findOne(codigo);
		if (obraSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return obraSalvo;
	}

	public Obra addAutor(Long codigo, Long codigoAutor) {
		Obra obraSalva = buscarPeloCodigo(codigo);
		Autor autor = buscarAutor(codigoAutor);
		obraSalva.addAutor(autor);
		autor.addObra(obraSalva);
		autorRepository.save(autor);
		return obraRepository.save(obraSalva);
	}

	public Page<Obra> filtrar(ObraFilter obraFilter, Pageable pageeble) {
		return obraRepository.filtrar(obraFilter, pageeble);
	}

	public void delete(Long codigo) {
		Obra obraSalvo = obraRepository.findOne(codigo);
		if (obraSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		obraRepository.delete(codigo);
	}

	private Autor buscarAutor(Long codigo) {
		Autor autorSalvo = autorRepository.findOne(codigo);
		if (autorSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return autorSalvo;
	}

}
