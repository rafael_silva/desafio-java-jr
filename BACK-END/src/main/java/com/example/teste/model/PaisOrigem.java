package com.example.teste.model;

public enum PaisOrigem {
	BRASIL("Brasil"), ARGENTINA("Argentina"), FRANCA("Franca");

	private String nome;
	
	private PaisOrigem(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
}
