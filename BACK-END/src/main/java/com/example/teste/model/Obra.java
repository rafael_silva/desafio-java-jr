package com.example.teste.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "obra")
public class Obra {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private String nome;
	
	@NotNull
	@Size(max = 240)
	private String descricao;
	
	@Column(name = "data_de_publicacao")
	private LocalDate dataDePublicacao;
	
	@Column(name = "data_de_exposicao")
	private LocalDate dataDeExposicao;
	
	
	@JoinTable(
			  name = "autor_obras", 
			  joinColumns = @JoinColumn(name = "obra_id"), 
			  inverseJoinColumns = @JoinColumn(name = "autor_id"))
	@JsonIgnoreProperties("obras")
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	private List<Autor> autores= new ArrayList<>();
	

	public void addAutor(Autor autor) {
		this.autores.add(autor);
	}
	
	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getDataDePublicacao() {
		return dataDePublicacao;
	}

	public void setDataDePublicacao(LocalDate dataDepublicacao) {
		this.dataDePublicacao = dataDepublicacao;
	}

	public LocalDate getDataDeExposicao() {
		return dataDeExposicao;
	}

	public void setDataDeExposicao(LocalDate dataDeexposicao) {
		this.dataDeExposicao = dataDeexposicao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obra other = (Obra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
	
}
