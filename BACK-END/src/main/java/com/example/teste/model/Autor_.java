package com.example.teste.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Autor.class)
public abstract class Autor_ {

	public static volatile SingularAttribute<Autor, String> cpf;
	public static volatile SingularAttribute<Autor, String> nome;
	public static volatile SingularAttribute<Autor, Long> id;
	public static volatile ListAttribute<Autor, Obra> obras;
	public static volatile SingularAttribute<Autor, Sexo> sexo;
	public static volatile SingularAttribute<Autor, Date> dataNascimento;
	public static volatile SingularAttribute<Autor, PaisOrigem> paisDeOrigem;
	public static volatile SingularAttribute<Autor, String> email;

}

