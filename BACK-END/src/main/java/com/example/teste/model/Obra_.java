package com.example.teste.model;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Obra.class)
public abstract class Obra_ {

	public static volatile SingularAttribute<Obra, LocalDate> dataDeExposicao;
	public static volatile SingularAttribute<Obra, String> nome;
	public static volatile SingularAttribute<Obra, Long> id;
	public static volatile ListAttribute<Obra, Autor> autores;
	public static volatile SingularAttribute<Obra, LocalDate> dataDePublicacao;
	public static volatile SingularAttribute<Obra, String> descricao;

}

