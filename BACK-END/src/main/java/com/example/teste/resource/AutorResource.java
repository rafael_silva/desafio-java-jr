package com.example.teste.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.teste.event.RecursoCriadoEvent;
import com.example.teste.exceptionhandler.DesafioException;
import com.example.teste.model.Autor;
import com.example.teste.service.AutorService;

@RestController
@RequestMapping("/autor")
public class AutorResource {

	@Autowired
	private AutorService autorService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	
	@GetMapping
	public List<Autor> listar(){
		return autorService.findAll();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Autor> buscarPeloCodigo(@PathVariable Long codigo){
		Autor autor = autorService.buscarPeloCodigo(codigo);
		return autor != null ? ResponseEntity.ok(autor) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<Autor> criar(@Valid @RequestBody Autor autor,HttpServletResponse response) throws DesafioException {
		Autor autorSalvo=autorService.save(autor);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, autor.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(autorSalvo);
	}	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		autorService.delete(codigo);
	}
	
	@PutMapping("/{codigo}/adicionarObra")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Autor> addObra(@PathVariable Long codigo, @RequestBody Long obra){
		Autor autorSalvo= autorService.addObra(codigo, obra);
		return ResponseEntity.ok(autorSalvo);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Autor> atualizar(@Valid @RequestBody Autor autor, @PathVariable Long codigo) throws DesafioException{
		Autor autorSalvo= autorService.atualizar(codigo, autor);
		return ResponseEntity.ok(autorSalvo);
	}
	
}
