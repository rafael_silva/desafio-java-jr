package com.example.teste.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.teste.event.RecursoCriadoEvent;
import com.example.teste.model.Obra;
import com.example.teste.repository.filter.ObraFilter;
import com.example.teste.service.ObraService;

@RestController
@RequestMapping("/obra")
public class ObraResource {

	@Autowired
	private ObraService obraService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	
	@GetMapping
	public Page<Obra> pesquisar(ObraFilter obraFilter, Pageable pageeble){
		return obraService.filtrar(obraFilter, pageeble);
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Obra> buscarPeloCodigo(@PathVariable Long codigo){
		Obra obra = obraService.buscarPeloCodigo(codigo);
		return obra != null ? ResponseEntity.ok(obra) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<Obra> criar(@Valid @RequestBody Obra obra,HttpServletResponse response) {
		Obra obraSalvo=obraService.save(obra);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, obra.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(obraSalvo);
	}	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		obraService.delete(codigo);
	}
	
	@PutMapping("/{codigo}/adicionarAutor")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Obra> addAutor(@PathVariable Long codigo, @RequestBody Long obra){
		Obra obraSalva= obraService.addAutor(codigo, obra);
		return ResponseEntity.ok(obraSalva);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Obra> atualizar(@Valid @RequestBody Obra obra, @PathVariable Long codigo){
		Obra obraSalvo= obraService.atualizar(codigo, obra);
		return ResponseEntity.ok(obraSalvo);
	}
	
}
