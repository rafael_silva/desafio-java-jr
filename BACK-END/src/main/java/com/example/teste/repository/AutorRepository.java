package com.example.teste.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teste.model.Autor;

public interface AutorRepository extends JpaRepository<Autor, Long>{

}
