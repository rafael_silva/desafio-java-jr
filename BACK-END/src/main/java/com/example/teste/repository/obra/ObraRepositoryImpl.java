package com.example.teste.repository.obra;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.teste.model.Obra;
import com.example.teste.model.Obra_;
import com.example.teste.repository.filter.ObraFilter;

public class ObraRepositoryImpl implements ObraRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Obra> filtrar(ObraFilter obraFilter, Pageable pageable) {
		CriteriaBuilder builder= manager.getCriteriaBuilder();
		CriteriaQuery<Obra> criteria= builder.createQuery(Obra.class);
		Root<Obra> root= criteria.from(Obra.class);
		
		//criar restriçoes
		Predicate[] predicates= criarRestricoes(obraFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Obra> query= manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<> (query.getResultList(), pageable, total(obraFilter));
	}




	private Predicate[] criarRestricoes(ObraFilter obraFilter, CriteriaBuilder builder, Root<Obra> root) {
		List<Predicate> predicates= new ArrayList<>();
		
		if(!StringUtils.isEmpty(obraFilter.getNome())) {
			predicates.add(builder.like(
					builder.lower(root.get(Obra_.nome)),"%" + obraFilter.getNome().toLowerCase()+"%"));
		}
		if(!StringUtils.isEmpty(obraFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get(Obra_.descricao)),"%" + obraFilter.getDescricao().toLowerCase()+"%"));
		}
		
		
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	private void adicionarRestricoesDePaginacao(TypedQuery<Obra> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistroPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistroPorPagina);
		
		
	}
	private Long total(ObraFilter obraFilter) {
		CriteriaBuilder builder= manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Obra> root = criteria.from(Obra.class);
		
		//criar restriçoes
		Predicate[] predicates= criarRestricoes(obraFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
