package com.example.teste.repository.obra;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.teste.model.Obra;
import com.example.teste.repository.filter.ObraFilter;

public interface ObraRepositoryQuery {
	
	public Page<Obra> filtrar(ObraFilter obrafilter, Pageable pageeble);

}
