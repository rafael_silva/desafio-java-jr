package com.example.teste.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teste.model.Obra;
import com.example.teste.repository.obra.ObraRepositoryQuery;

public interface ObraRepository extends JpaRepository<Obra, Long >, ObraRepositoryQuery{

}
